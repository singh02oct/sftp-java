package javasftp.javasftp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class TestJSch {
    public static void main(String args[]) throws IOException {
        JSch jsch = new JSch();
        Session session = null;
        try {
            session = jsch.getSession("demo", "test.rebex.net", 22);
           //session.setConfig("StrictHostKeyChecking", "no");
            java.util.Properties config = new java.util.Properties();
            config.put("kex", "diffie-hellman-group1-sha1,diffie-hellman-group14-sha1,diffie-hellman-group-exchange-sha1,diffie-hellman-group-exchange-sha256");
            config.put("StrictHostKeyChecking", "no");

            session.setConfig(config);
            
            session.setPassword("password");
            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            System.out.println("Is Connected "+session.isConnected());
            
            String home = sftpChannel.getHome();
            System.out.println("home " + home);
            sftpChannel.cd("/");
            Vector filelist = sftpChannel.ls("/");
            for(int i=0; i<filelist.size();i++){
                System.out.println(filelist.get(i).toString());
            }
            
            InputStream stream = sftpChannel.get("/readme.txt");
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(stream));
                String line;
                while ((line = br.readLine()) != null) {
                    System.out.println(line);
                }

            } catch (IOException io) {
                System.out.println("Exception occurred during reading file from SFTP server due to " + io.getMessage());
                io.getMessage();

            } catch (Exception e) {
                System.out.println("Exception occurred during reading file from SFTP server due to " + e.getMessage());
                e.getMessage();

            }

            sftpChannel.exit();
            session.disconnect();
        } catch (JSchException e) {
            e.printStackTrace();
        } catch (SftpException e) {
            e.printStackTrace();
        }
    }
}

